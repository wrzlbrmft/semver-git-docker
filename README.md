# docker-semver-git

The semantic versioner for npm with Git for CI/CD pipelines in Docker

```
docker pull wrzlbrmft/semver-git:latest
```

See also:

  * https://www.npmjs.com/package/semver
  * https://git-scm.com/
  * https://hub.docker.com/r/wrzlbrmft/semver-git/tags

## License

The content of this repository is distributed under the terms of the
[GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html).
