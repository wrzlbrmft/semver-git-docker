FROM alpine:latest

RUN apk add --no-cache \
        ca-certificates \
        git \
        jq \
        moreutils \
        patch \
        xmlstarlet \
        yarn \
        yq

RUN yarn global add semver
